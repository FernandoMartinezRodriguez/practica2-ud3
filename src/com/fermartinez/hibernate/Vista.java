package com.fermartinez.hibernate;

import com.github.lgooddatepicker.components.DatePicker;

import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import java.awt.*;

public class Vista extends JFrame{
    private final static String TITULOFRAME = "Restaurantes";
    private JTabbedPane tabbedPane1;
    private JPanel panel1;
    JTextField txtNombreCocinero;
    JTextField txtApellidoCocinero;
    JTextField txtDniCocinero;
    JList list1;
    JButton btnListarC;
    JButton btnNuevoC;
    JButton btnModificarC;
    JTextField txtNombrePlato;
    JComboBox comboAlergenos;
    JList list2;
    JButton btnListarP;
    JButton btnNuevoP;
    JButton btnModificarP;
    JTextField txtNombreMenu;
    JTextField txtPostre;
    JTextField txtPrecioMenu;
    JList list3;
    JButton btnEliminarM;
    JButton btnNuevoM;
    JButton btnModificarM;
    JTextField txtNombreRes;
    JTextField txtDireccionRes;
    JTextField txtTelefonoRes;
    JComboBox comboEstrellas;
    JList list4;
    JButton btnEliminarRes;
    JButton btnNuevoRes;
    JButton btnModificarRes;
    JLabel lblNombreCocinero;
    JPanel lblApellidoCocinero;
    JLabel lblDNICocinero;
    JLabel lblNombrePlato;
    JPanel lblAlergenos;
    JLabel lblPrecioPlato;
    JTextField txtPrecioPlato;
    JLabel lblNombreMenu;
    JLabel lblPostreMenu;
    JLabel lblPrecioMenu;
    JLabel lblFechaMenu;
    DatePicker fecha;
    JLabel lblNombreRes;
    JLabel lblEstrellasRes;
    JLabel lblDireccionRes;
    JLabel lblTelefonoRes;
    JComboBox comboCocinero;
    JComboBox comboRestaurante;
    JList list5;
    JButton btnEliminarTrabajador;
    JButton btnNuevoTrabajador;
    JButton btnModTrabajador;
    DatePicker fechaInicioTrabajador;
    DatePicker fechaFinTrabajador;
    JComboBox comboMenuCarta;
    JComboBox comboResCarta;
    JList list6;
    JButton btnEliminarCarta;
    JButton btnNuevoCarta;
    JButton btnModCarta;
    JLabel lblFechaCreacionCarta;
    JLabel lblMenuCarta;
    JLabel lblResCarta;
    DatePicker fechaCarta;
    private JButton btnEliminarC;
    private JButton btnEliminarP;

    DefaultTableModel dtmCocinero;
    DefaultTableModel dtmPlato;
    DefaultTableModel dtmMenu;
    DefaultTableModel dtmRestaurante;

    /*MENUBAR*/
    JMenuItem itemOpciones;
    JMenuItem itemDesconectar;
    JMenuItem itemSalir;
    /*SAVECHANGESDIALOG*/
    JDialog adminPasswordDialog;
    JButton btnValidar;
    JPasswordField AdminPassword;

    JMenuItem conexionItem;
    JMenuItem salirItem;
    /**
     * Constructor de la clase.
     * Setea el título de la ventana y llama al metodo que la inicia
     */
    public Vista() {
        super(TITULOFRAME);
        initFrame();
    }

    /**
     * Configura la ventana y la hace visible
     */
    private void initFrame() {
        this.setContentPane(panel1);
        this.setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
        this.pack();
        this.setVisible(true);
        this.setSize(new Dimension(this.getWidth()+200, this.getHeight()+100));
        this.setLocationRelativeTo(null);
        optionDialog = new OptionDialog(this);
        setMenu();
        setAdminDialog();
        setEnumComboBox();
        setTableModels();
    }

    /**
     * Inicializa los DefaultTableModel, los setea en sus respectivas tablas
     */
    private void setTableModels() {

        this.dtmCocinero = new DefaultTableModel();
        this.list1.setModel((ListModel) dtmCocinero);

        this.dtmPlato = new DefaultTableModel();
        this.list2.setModel((ListModel) dtmPlato);

        this.dtmMenu = new DefaultTableModel();
        this.list3.setModel((ListModel) dtmMenu);

        this.dtmRestaurante = new DefaultTableModel();
        this.list4.setModel((ListModel) dtmRestaurante);
    }

    /**
     * Setea una barra de menus
     */
    private void setMenu(){
        JMenuBar mbBar = new JMenuBar();
        JMenu menu = new JMenu("Archivo");
        itemOpciones = new JMenuItem("Opciones");
        itemOpciones.setActionCommand("Opciones");
        itemDesconectar = new JMenuItem("Desconectar");
        itemDesconectar.setActionCommand("Desconectar");
        itemSalir = new JMenuItem("Salir");
        itemSalir.setActionCommand("Salir");
        menu.add(itemOpciones);
        menu.add(itemDesconectar);
        menu.add(itemSalir);
        mbBar.add(menu);
        mbBar.add(Box.createHorizontalGlue());
        this.setJMenuBar(mbBar);
    }

    /**
     * Setea los items de cbEType, de cbBGenre y de cbTables con sus respectivas enumeraciones
     */
    private void setEnumComboBox() {
        for(Alergenos constant : Alergenos.values()) { comboAlergenos.addItem(constant.getValor()); }
        comboAlergenos.setSelectedIndex(-1);
        for(Estrellas constant : Estrellas.values()) { comboEstrellas.addItem(constant.getValor()); }
        comboEstrellas.setSelectedIndex(-1);
    }

    /**
     * Setea un JDialog para introducir la contreña de administrador y poder configurar la com.ferMartinez.practica2.base de datos
     */
    private void setAdminDialog() {
        btnValidar = new JButton("Validar");
        btnValidar.setActionCommand("abrirOpciones");
        AdminPassword = new JPasswordField();
        AdminPassword.setPreferredSize(new Dimension(100, 26));
        Object[] options = new Object[] {AdminPassword, btnValidar};
        JOptionPane jop = new JOptionPane("Introduce la contraseña"
                , JOptionPane.WARNING_MESSAGE, JOptionPane.YES_NO_OPTION, null, options);

        adminPasswordDialog = new JDialog(this, "Opciones", true);
        adminPasswordDialog.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
        adminPasswordDialog.setContentPane(jop);
        adminPasswordDialog.pack();
        adminPasswordDialog.setLocationRelativeTo(this);

    }
}
