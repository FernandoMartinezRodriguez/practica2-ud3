package com.fermartinez.hibernate;

import javax.persistence.*;
import java.util.List;
import java.util.Objects;

@Entity
@Table(name = "platos", schema = "basehibernate", catalog = "")
public class Plato {
    private int idPlato;
    private String nombre;
    private String alergenos;
    private String descripcion;
    private String especias;
    private double precio;
    private Cocinero cocinero;
    private List<Menu> menus;

    @Id
    @Column(name = "id_plato")
    public int getIdPlato() {
        return idPlato;
    }

    public void setIdPlato(int idPlato) {
        this.idPlato = idPlato;
    }

    @Basic
    @Column(name = "nombre")
    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    @Basic
    @Column(name = "alergenos")
    public String getAlergenos() {
        return alergenos;
    }

    public void setAlergenos(String alergenos) {
        this.alergenos = alergenos;
    }

    @Basic
    @Column(name = "descripcion")
    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    @Basic
    @Column(name = "especias")
    public String getEspecias() {
        return especias;
    }

    public void setEspecias(String especias) {
        this.especias = especias;
    }

    @Basic
    @Column(name = "precio")
    public double getPrecio() {
        return precio;
    }

    public void setPrecio(double precio) {
        this.precio = precio;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Plato plato = (Plato) o;
        return idPlato == plato.idPlato &&
                Double.compare(plato.precio, precio) == 0 &&
                Objects.equals(nombre, plato.nombre) &&
                Objects.equals(alergenos, plato.alergenos) &&
                Objects.equals(descripcion, plato.descripcion) &&
                Objects.equals(especias, plato.especias);
    }

    @Override
    public int hashCode() {
        return Objects.hash(idPlato, nombre, alergenos, descripcion, especias, precio);
    }

    @ManyToOne
    @JoinColumn(name = "id_cocinero", referencedColumnName = "id_cocinero", nullable = false)
    public Cocinero getCocinero() {
        return cocinero;
    }

    public void setCocinero(Cocinero cocinero) {
        this.cocinero = cocinero;
    }

    @ManyToMany(mappedBy = "platos")
    public List<Menu> getMenus() {
        return menus;
    }

    public void setMenus(List<Menu> menus) {
        this.menus = menus;
    }
}
