package com.fermartinez.hibernate;

import javax.swing.*;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.table.DefaultTableModel;
import java.awt.event.*;
import java.sql.*;
import java.util.Vector;

public class Controlador implements ActionListener, ItemListener, ListSelectionListener, WindowListener {

    private Modelo modelo;
    private Vista vista;
    boolean refrescar;

    /**
     * Constructor de la clase, inicializa el modelo y la vista, carga los datos
     * desde un fichero, añade los listeners y lista los datos.
     *
     * @param modelo Modelo
     * @param vista  Vista
     */
    public Controlador(Modelo modelo, Vista vista) {
        this.modelo = modelo;
        this.vista = vista;
        modelo.conectar();
        setOptions();
        addActionListeners(this);
        addItemListeners(this);
        addWindowListeners(this);
    }



    /**
     * Añade ActionListeners a los botones y menus de la vista
     * @param listener ActionListener que se añade
     */
    private void addActionListeners(ActionListener listener) {
        vista.btnListarC.addActionListener(listener);
        vista.btnListarP.addActionListener(listener);
        vista.btnEliminarM.addActionListener(listener);
        vista.btnEliminarRes.addActionListener(listener);
        vista.btnNuevoC.addActionListener(listener);
        vista.btnNuevoP.addActionListener(listener);
        vista.btnNuevoM.addActionListener(listener);
        vista.btnNuevoRes.addActionListener(listener);
        vista.btnModificarC.addActionListener(listener);
        vista.btnModificarM.addActionListener(listener);
        vista.btnModificarP.addActionListener(listener);
        vista.btnModificarRes.addActionListener(listener);
        vista.optionDialog.btnGuardar.addActionListener(listener);
        vista.itemOpciones.addActionListener(listener);
        vista.itemSalir.addActionListener(listener);
        vista.btnValidar.addActionListener(listener);
    }

    /**
     * Añade WindowListeners a la vista
     * @param listener WindowListener que se añade
     */
    private void addWindowListeners(WindowListener listener) {
        vista.addWindowListener(listener);
    }

    /**
     * Muestra los atributos de un objeto seleccionado y los borra una vez se deselecciona
     * @param e Evento producido en una lista
     */

    @Override
    public void valueChanged(ListSelectionEvent e) {
        if(e.getValueIsAdjusting()){
            if(e.getSource() == vista.list1) {
                Cocinero cocinero = (Cocinero) vista.list1.getSelectedValue();
                vista.txtNombreCocinero.setText(String.valueOf(cocinero.getNombre()));
                vista.txtApellidoCocinero.setText(cocinero.getApellidos());
                vista.txtDniCocinero.setText(cocinero.getDni());


            }else if(e.getSource() == vista.list2){
                Plato platoSeleccionado = (Plato) vista.list2.getSelectedValue();
                vista.txtNombrePlato.setText(String.valueOf(platoSeleccionado.getNombre()));
                vista.txtPrecioPlato.setText(String.valueOf(platoSeleccionado.getPrecio()));

            }else if(e.getSource() == vista.list3){
                Menu menuSeleccionado = (Menu) vista.list3.getSelectedValue();
                vista.txtNombreMenu.setText(String.valueOf(menuSeleccionado.getNombre()));
                vista.txtPostre.setText(String.valueOf(menuSeleccionado.getPostre()));
                vista.txtPrecioMenu.setText(String.valueOf(menuSeleccionado.getPrecio()));
                vista.fecha.setDate((Date.valueOf(String.valueOf(menuSeleccionado.getFechaCreacion()))).toLocalDate());


            }else if(e.getSource() == vista.list4){
                Estrellas es = (Estrellas) vista.list4.getSelectedValue();//Esto para los combos de enumeraciones NOSE SI ESTA BIEN
                Restaurante resSeleccionado = (Restaurante) vista.list4.getSelectedValue();
                vista.txtNombreRes.setText(String.valueOf(resSeleccionado.getNombre()));
                vista.txtDireccionRes.setText(String.valueOf(resSeleccionado.getDireccion()));
                vista.txtTelefonoRes.setText(String.valueOf(resSeleccionado.getTelefono()));
                vista.comboEstrellas.setSelectedItem(String.valueOf(es.getValor()));

            }else if(e.getSource() == vista.list5){
                CocineroRestaurante trabajador = (CocineroRestaurante) vista.list5.getSelectedValue();
                vista.fechaInicioTrabajador.setDate((Date.valueOf(String.valueOf(trabajador.getFechaInicio()))).toLocalDate());
                vista.fechaFinTrabajador.setDate((Date.valueOf(String.valueOf(trabajador.getFechaFin()))).toLocalDate());
                vista.comboCocinero.setSelectedItem(String.valueOf(trabajador.getCocinero()));
                vista.comboRestaurante.setSelectedItem(String.valueOf(trabajador.getRestaurante()));
            }
            }else if(e.getSource() == vista.list6){
                MenuRestaurante carta = (MenuRestaurante) vista.list6.getSelectedValue();
                vista.fechaCarta.setDate((Date.valueOf(String.valueOf(carta.getFecha()))).toLocalDate());
                vista.comboMenuCarta.setSelectedItem(String.valueOf(carta.getMenu()));
                vista.comboResCarta.setSelectedItem(String.valueOf(carta.getRestaurante()));
            }

    }
    /**
     * Añade sus respectivas acciones a los botones de añadir, modificar y eliinar de las listas.
     * Añade sus respectivas acciones a los menus de guardar, guardar como y mostrar opciones.
     * Añade la acción de guardar las opciones al dialog de opciones.
     * Añade las acciones de guardar, no guardar y cancelar a los botones del dialog de guardar
     *  cambios cuando se intenta salir de la aplicación una vez se ha desactivado el autoguardado.
     * @param e Evento producido al pulsar un botón o menu
     */
    @Override
    public void actionPerformed(ActionEvent e) {
        String comando = e.getActionCommand();

        switch(comando){
            case "Salir":
                modelo.desconectar();
                System.exit(0);
                break;
            case "Conectar":
                vista.conexionItem.setEnabled(false);
                modelo.conectar();
                break;

            case "anadirCocinero":
                Cocinero nuevoCocinero = new Cocinero();
                nuevoCocinero.setNombre(vista.txtNombreCocinero.getText());
                nuevoCocinero.setApellidos(vista.txtApellidoCocinero.getText());
                nuevoCocinero.setDni(vista.txtDniCocinero.getText());
                modelo.insertarCocinero(nuevoCocinero);
                break;

            case "anadirPlato":
                Plato nuevoPlato = new Plato();
                nuevoPlato.setNombre(vista.txtNombrePlato.getText());
                nuevoPlato.setAlergenos(String.valueOf(vista.comboAlergenos.getSelectedItem()));
                nuevoPlato.setPrecio(Double.parseDouble(vista.txtPrecioPlato.getText()));
                modelo.insertarPlato(nuevoPlato);
                break;

            case "anadirMenu":
                Menu nuevoMenu = new Menu();
                nuevoMenu.setNombre(vista.txtNombreMenu.getText());
                nuevoMenu.setPostre(vista.txtPostre.getText());
                nuevoMenu.setPrecio(Double.parseDouble(vista.txtPrecioMenu.getText()));
                nuevoMenu.setFechaCreacion(Date.valueOf(vista.fecha.getText()));
                modelo.insertarMenu(nuevoMenu);
                break;

            case "anadirRes":
                Restaurante nuevoRestaurante = new Restaurante();
                nuevoRestaurante.setNombre(vista.txtNombrePlato.getText());
                nuevoRestaurante.setEstrellas((Integer) vista.comboEstrellas.getSelectedItem());
                nuevoRestaurante.setDireccion(vista.txtDireccionRes.getText());
                nuevoRestaurante.setTelefono(vista.txtTelefonoRes.getText());
                modelo.insertarRestaurante(nuevoRestaurante);
                break;

            case "anadirTrabajador":
                CocineroRestaurante nuevoTrabajador = new CocineroRestaurante();
                nuevoTrabajador.setFechaInicio(Date.valueOf(vista.fechaInicioTrabajador.getText()));
                nuevoTrabajador.setFechaFin(Date.valueOf(vista.fechaFinTrabajador.getText()));
                nuevoTrabajador.setCocinero((Cocinero) vista.comboCocinero.getSelectedItem());
                nuevoTrabajador.setRestaurante((Restaurante) vista.comboRestaurante.getSelectedItem());
                modelo.insertarTrabajador(nuevoTrabajador);
                break;

            case "anadirCarta":
                MenuRestaurante nuevaCarta = new MenuRestaurante();
                nuevaCarta.setFecha(Date.valueOf(vista.fechaCarta.getText()));
                nuevaCarta.setMenu((Menu) vista.comboMenuCarta.getSelectedItem());
                nuevaCarta.setRestaurante((Restaurante) vista.comboResCarta.getSelectedItem());
                modelo.insertarCarta(nuevaCarta);
                break;

            case "Listar":
                listarCoches(modelo.getCoches());
                break;

            case "ModificarCocinero":
                Cocinero cocineroSeleccion = (Cocinero) vista.list1.getSelectedValue();
                cocineroSeleccion.setNombre(vista.txtNombreCocinero.getText());
                cocineroSeleccion.setApellidos(vista.txtApellidoCocinero.getText());
                cocineroSeleccion.setDni(vista.txtDniCocinero.getText());
                modelo.modificarCocinero(cocineroSeleccion);
                break;

            case "ModificarPlato":
                Plato platoSeleccion = (Plato) vista.list2.getSelectedValue();
                platoSeleccion.setNombre(vista.txtNombrePlato.getText());
                platoSeleccion.setAlergenos(String.valueOf(vista.comboAlergenos.getSelectedItem()));
                platoSeleccion.setPrecio(Double.parseDouble(vista.txtPrecioPlato.getText()));
                modelo.modificarPlato(platoSeleccion);
                break;

            case "ModificarMenu":
                Menu menuSeleccion = (Menu) vista.list3.getSelectedValue();
                menuSeleccion.setNombre(vista.txtNombreMenu.getText());
                menuSeleccion.setPostre(vista.txtPostre.getText());
                menuSeleccion.setPrecio(Double.parseDouble(vista.txtPrecioMenu.getText()));
                menuSeleccion.setFechaCreacion(Date.valueOf((vista.fecha.getText())));
                modelo.modificarMenu(menuSeleccion);
                break;

            case "ModificarRes":
                Restaurante restauranteSeleccion = (Restaurante) vista.list4.getSelectedValue();
                restauranteSeleccion.setNombre(vista.txtNombreRes.getText());
                restauranteSeleccion.setEstrellas((Integer) vista.comboEstrellas.getSelectedItem()));
                restauranteSeleccion.setDireccion(vista.txtDireccionRes.getText());
                restauranteSeleccion.setTelefono(vista.txtTelefonoRes.getText());
                modelo.modificarRestaurante(restauranteSeleccion);
                break;

            case "ModificarTrabajador":
                CocineroRestaurante trabajadorSeleccion = (CocineroRestaurante) vista.list5.getSelectedValue();
                trabajadorSeleccion.setFechaFin(Date.valueOf((vista.fechaFinTrabajador.getText())));
                trabajadorSeleccion.setFechaInicio(Date.valueOf((vista.fechaInicioTrabajador.getText())));
                trabajadorSeleccion.setCocinero((Cocinero) vista.comboCocinero.getSelectedItem());
                trabajadorSeleccion.setRestaurante((Restaurante) vista.comboRestaurante.getSelectedItem());
                modelo.modificarTrabajador(trabajadorSeleccion);
                break;

            case "ModificarCarta":
                MenuRestaurante cartaSeleccion = (MenuRestaurante) vista.list6.getSelectedValue();
                cartaSeleccion.setFecha(Date.valueOf((vista.fechaCarta.getText())));
                cartaSeleccion.setMenu((Menu) vista.comboMenuCarta.getSelectedItem());
                cartaSeleccion.setRestaurante((Restaurante) vista.comboResCarta.getSelectedItem());
                modelo.modificarCarta(cartaSeleccion);
                break;

            case "EliminarCocinero":
                Cocinero cocineroBorrado  = (Cocinero) vista.list1.getSelectedValue();
                modelo.borrarCocinero(cocineroBorrado);
                break;

            case "EliminarPlato":
                Plato platoBorrado  = (Plato) vista.list2.getSelectedValue();
                modelo.borrarPlato(platoBorrado);
                break;

            case "EliminarMenu":
                Menu menuBorrado  = (Menu) vista.list3.getSelectedValue();
                modelo.borrarMenu(menuBorrado);
                break;

            case "EliminarRes":
                Restaurante restauranteBorrado  = (Restaurante) vista.list4.getSelectedValue();
                modelo.borrarRestaurante(restauranteBorrado);
                break;

            case "EliminarTrabajador":
                CocineroRestaurante trabajadorBorrado  = (CocineroRestaurante) vista.list5.getSelectedValue();
                modelo.borrarTrabajador(trabajadorBorrado);
                break;

            case "EliminarCarta":
                MenuRestaurante cartaBorrada  = (MenuRestaurante) vista.list6.getSelectedValue();
                modelo.borrarCarta(cartaBorrada);
                break;
            case "ListarPropietarios":
                listarPropietarios(modelo.getPropietarios());
                break;


        }

        listarCoches(modelo.getCoches());
    }

    @Override
    public void windowOpened(WindowEvent e) {

    }

    /**
     * Añade acciones cuando la ventana se cierra. Si el autoguardado está activado la ventana se cerrará
     * sin más, volcando todos los datos en el fichero. De lo contrario pueden ocurrir dos cosas:
     * Si todos los cambios han sido guardados, la ventana se cerrará. Si no lo están, aparecerá una ventana
     * que permitirá al usuario decidir que hacer con esos cambios no guardados o bien cancelar el cierre
     * de la aplicación.
     * @param e Evento producido al tratar de cerrar la ventana
     */
    @Override
    public void windowClosing(WindowEvent e) {
        System.exit(0);
    }

    @Override
    public void windowClosed(WindowEvent e) {

    }

    @Override
    public void windowIconified(WindowEvent e) {

    }

    @Override
    public void windowDeiconified(WindowEvent e) {

    }

    @Override
    public void windowActivated(WindowEvent e) {

    }

    @Override
    public void windowDeactivated(WindowEvent e) {

    }

    /**
     * Actualiza los primeros platos que se ven en la lista y los comboboxes
     */
    private void refrescarPlato() {
        try {
            vista.list2.setModel((ListModel) construirTableModelPlato(modelo.consultarPlato()));
            vista.comboPrimero.removeAllItems();
            vista.comboSegundo.removeAllItems();
            for(int i = 0; i < vista.dtmPlato.getRowCount(); i++) {
                vista.comboPrimero.addItem(vista.dtmPlato.getValueAt(i, 0)+" - "+
                        vista.dtmPlato.getValueAt(i, 1));
            }
            for(int i = 0; i < vista.dtmPlato.getRowCount(); i++) {
                vista.comboSegundo.addItem(vista.dtmPlato.getValueAt(i, 0)+" - "+
                        vista.dtmPlato.getValueAt(i, 1));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    private DefaultTableModel construirTableModelPlato(ResultSet rs)
            throws SQLException {

        ResultSetMetaData metaData = rs.getMetaData();

        // names of columns
        Vector<String> columnNames = new Vector<>();
        int columnCount = metaData.getColumnCount();
        for (int column = 1; column <= columnCount; column++) {
            columnNames.add(metaData.getColumnName(column));
        }

        // data of the table
        Vector<Vector<Object>> data = new Vector<>();
        setDataVector(rs, columnCount, data);

        vista.dtmPlato.setDataVector(data, columnNames);

        return vista.dtmPlato;

    }
    /**
     * Actualiza los menus que se ven en la lista y los comboboxes
     */
    private void refrescarCocinero() {
        try {
            vista.list1.setModel((ListModel) construirTableModelCocinero(modelo.consultarCocinero()));
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    private DefaultTableModel construirTableModelCocinero(ResultSet rs)
            throws SQLException {

        ResultSetMetaData metaData = rs.getMetaData();

        // names of columns
        Vector<String> columnNames = new Vector<>();
        int columnCount = metaData.getColumnCount();
        for (int column = 1; column <= columnCount; column++) {
            columnNames.add(metaData.getColumnName(column));
        }

        // data of the table
        Vector<Vector<Object>> data = new Vector<>();
        setDataVector(rs, columnCount, data);

        vista.dtmCocinero.setDataVector(data, columnNames);

        return vista.dtmCocinero;

    }

    /**
     * Actualiza los menus que se ven en la lista y los comboboxes
     */
    private void refrescarMenu() {
        try {
            vista.list3.setModel((ListModel) construirTableModelMenu(modelo.consultarMenu()));
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    private DefaultTableModel construirTableModelMenu(ResultSet rs)
            throws SQLException {

        ResultSetMetaData metaData = rs.getMetaData();

        // names of columns
        Vector<String> columnNames = new Vector<>();
        int columnCount = metaData.getColumnCount();
        for (int column = 1; column <= columnCount; column++) {
            columnNames.add(metaData.getColumnName(column));
        }

        // data of the table
        Vector<Vector<Object>> data = new Vector<>();
        setDataVector(rs, columnCount, data);

        vista.dtmMenu.setDataVector(data, columnNames);

        return vista.dtmMenu;

    }

    /**
     * Actualiza los menus que se ven en la lista y los comboboxes
     */
    private void refrescarRestaurante() {
        try {
            vista.list4.setModel((ListModel) construirTableModelResturante(modelo.consultarRestaurantes()));
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    private DefaultTableModel construirTableModelResturante(ResultSet rs)
            throws SQLException {

        ResultSetMetaData metaData = rs.getMetaData();

        // names of columns
        Vector<String> columnNames = new Vector<>();
        int columnCount = metaData.getColumnCount();
        for (int column = 1; column <= columnCount; column++) {
            columnNames.add(metaData.getColumnName(column));
        }

        // data of the table
        Vector<Vector<Object>> data = new Vector<>();
        setDataVector(rs, columnCount, data);

        vista.dtmRestaurante.setDataVector(data, columnNames);

        return vista.dtmRestaurante;

    }

    private void setDataVector(ResultSet rs, int columnCount, Vector<Vector<Object>> data) throws SQLException {
        while (rs.next()) {
            Vector<Object> vector = new Vector<>();
            for (int columnIndex = 1; columnIndex <= columnCount; columnIndex++) {
                vector.add(rs.getObject(columnIndex));
            }
            data.add(vector);
        }
    }

    /**
     * Setea el dialog de opciones según las opciones que el usuario guardo en la
     * última ejecución del programa.
     */
    private void setOptions() {
        vista.optionDialog.txtIP.setText(modelo.getIP());
        vista.optionDialog.txtUser.setText(modelo.getUser());
        vista.optionDialog.txtPasword.setText(modelo.getPassword());
        vista.optionDialog.txtAdmin.setText(modelo.getAdminPassword());
    }

    /**
     * Vacía los campos de la tab de menus
     */
    private void borrarCamposMenu() {
        vista.comboPrimero.setSelectedIndex(-1);
        vista.comboSegundo.setSelectedIndex(-1);
        vista.txtNombreMenu.setText("");
        vista.txtPostre.setText("");
        vista.txtPrecioMenu.setText("");
        vista.fecha.setText("");
    }

    /**
     * Vacía los campos de la tab de primeros platos
     */
    private void borrarCamposPlato() {
        vista.txtNombrePlato.setText("");
        vista.comboAlergenos.setSelectedIndex(-1);
        vista.txtPrecioPlato.setText("");
    }

    /**
     * Vacía los campos de la tab de segundos platos
     */
    private void borrarCamposCocinero() {
        vista.txtNombreCocinero.setText("");
        vista.txtApellidoCocinero.setText("");
        vista.txtDniCocinero.setText("");
    }

    private void borrarCamposRestaurante() {
        vista.txtNombreRes.setText("");
        vista.comboEstrellas.setSelectedIndex(-1);
        vista.txtDireccionRes.setText("");
        vista.txtTelefonoRes.setText("");
    }

    /**
     * Comprueba que los campos necesarios para añadir un menú estén vacíos
     *
     * @return True si al menos uno de los campos está vacío
     */
    private boolean comprobarMenuVacio() {
        return vista.txtNombreMenu.getText().isEmpty() ||
                vista.txtPrecioMenu.getText().isEmpty() ||
                vista.txtPostre.getText().isEmpty() ||
                vista.comboSegundo.getSelectedIndex() == -1 ||
                vista.comboPrimero.getSelectedIndex() == -1 ||
                vista.fecha.getText().isEmpty();
    }

    /**
     * Comprueba que los campos necesarios para añadir un primer plato estén vacíos
     *
     * @return True si al menos uno de los campos está vacío
     */
    private boolean comprobarPlatoVacio() {
        return vista.txtNombrePlato.getText().isEmpty() ||
                vista.txtPrecioPlato.getText().isEmpty() ||
                vista.comboAlergenos.getSelectedIndex() == -1 ;
    }

    /**
     * Comprueba que los campos necesarios para añadir un segundo plato estén vacíos
     *
     * @return True si al menos uno de los campos está vacío
     */
    private boolean comprobarCocineroVacio() {
        return vista.txtNombreCocinero.getText().isEmpty() ||
                vista.txtApellidoCocinero.getText().isEmpty() ||
                vista.txtDniCocinero.getText().isEmpty();
    }

    private boolean comprobarRestauranteVacio() {
        return vista.txtNombreRes.getText().isEmpty() ||
                vista.txtDireccionRes.getText().isEmpty() ||
                vista.comboEstrellas.getSelectedIndex() == -1 ||
                vista.txtTelefonoRes.getText().isEmpty();
    }

    /*LISTENERS IPLEMENTOS NO UTILIZADOS*/

    private void addItemListeners(Controlador controlador) {
    }

    @Override
    public void itemStateChanged(ItemEvent e) {

    }
}
