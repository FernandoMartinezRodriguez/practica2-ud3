package com.fermartinez.hibernate;

import javax.persistence.*;
import java.util.List;
import java.util.Objects;

@Entity
@Table(name = "restaurantes", schema = "basehibernate", catalog = "")
public class Restaurante {
    private int idRestaurante;
    private String nombre;
    private int estrellas;
    private String direccion;
    private String telefono;
    private CocineroRestaurante trabaja;
    private List<MenuRestaurante> sirve;

    @Id
    @Column(name = "id_restaurante")
    public int getIdRestaurante() {
        return idRestaurante;
    }

    public void setIdRestaurante(int idRestaurante) {
        this.idRestaurante = idRestaurante;
    }

    @Basic
    @Column(name = "nombre")
    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    @Basic
    @Column(name = "estrellas")
    public int getEstrellas() {
        return estrellas;
    }

    public void setEstrellas(int estrellas) {
        this.estrellas = estrellas;
    }

    @Basic
    @Column(name = "direccion")
    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    @Basic
    @Column(name = "telefono")
    public String getTelefono() {
        return telefono;
    }

    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Restaurante that = (Restaurante) o;
        return idRestaurante == that.idRestaurante &&
                estrellas == that.estrellas &&
                Objects.equals(nombre, that.nombre) &&
                Objects.equals(direccion, that.direccion) &&
                Objects.equals(telefono, that.telefono);
    }

    @Override
    public int hashCode() {
        return Objects.hash(idRestaurante, nombre, estrellas, direccion, telefono);
    }

    @OneToOne
    @JoinColumn(name = "id_restaurante", referencedColumnName = "id_restaurante", nullable = false)
    public CocineroRestaurante getTrabaja() {
        return trabaja;
    }

    public void setTrabaja(CocineroRestaurante trabaja) {
        this.trabaja = trabaja;
    }

    @OneToMany(mappedBy = "restaurante")
    public List<MenuRestaurante> getSirve() {
        return sirve;
    }

    public void setSirve(List<MenuRestaurante> sirve) {
        this.sirve = sirve;
    }
}
