package com.fermartinez.hibernate;

import javax.persistence.*;
import java.util.List;
import java.util.Objects;

@Entity
@Table(name = "cocineros", schema = "basehibernate", catalog = "")
public class Cocinero {
    private int idCocinero;
    private String dni;
    private String nombre;
    private String apellidos;
    private String puesto;
    private int telefono;
    private List<Plato> platos;
    private List<CocineroRestaurante> trabaja;

    @Id
    @Column(name = "id_cocinero")
    public int getIdCocinero() {
        return idCocinero;
    }

    public void setIdCocinero(int idCocinero) {
        this.idCocinero = idCocinero;
    }

    @Basic
    @Column(name = "dni")
    public String getDni() {
        return dni;
    }

    public void setDni(String dni) {
        this.dni = dni;
    }

    @Basic
    @Column(name = "nombre")
    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    @Basic
    @Column(name = "apellidos")
    public String getApellidos() {
        return apellidos;
    }

    public void setApellidos(String apellidos) {
        this.apellidos = apellidos;
    }

    @Basic
    @Column(name = "puesto")
    public String getPuesto() {
        return puesto;
    }

    public void setPuesto(String puesto) {
        this.puesto = puesto;
    }

    @Basic
    @Column(name = "telefono")
    public int getTelefono() {
        return telefono;
    }

    public void setTelefono(int telefono) {
        this.telefono = telefono;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Cocinero cocinero = (Cocinero) o;
        return idCocinero == cocinero.idCocinero &&
                telefono == cocinero.telefono &&
                Objects.equals(dni, cocinero.dni) &&
                Objects.equals(nombre, cocinero.nombre) &&
                Objects.equals(apellidos, cocinero.apellidos) &&
                Objects.equals(puesto, cocinero.puesto);
    }

    @Override
    public int hashCode() {
        return Objects.hash(idCocinero, dni, nombre, apellidos, puesto, telefono);
    }

    @OneToMany(mappedBy = "cocinero")
    public List<Plato> getPlatos() {
        return platos;
    }

    public void setPlatos(List<Plato> platos) {
        this.platos = platos;
    }

    @OneToMany(mappedBy = "cocinero")
    public List<CocineroRestaurante> getTrabaja() {
        return trabaja;
    }

    public void setTrabaja(List<CocineroRestaurante> trabaja) {
        this.trabaja = trabaja;
    }
}
