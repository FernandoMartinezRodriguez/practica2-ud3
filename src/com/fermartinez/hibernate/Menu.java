package com.fermartinez.hibernate;

import javax.persistence.*;
import java.sql.Date;
import java.util.List;
import java.util.Objects;

@Entity
@Table(name = "menus", schema = "basehibernate", catalog = "")
public class Menu {
    private int idMenu;
    private String nombre;
    private String postre;
    private double precio;
    private Date fechaCreacion;
    private List<MenuRestaurante> sirve;
    private List<Plato> platos;

    @Id
    @Column(name = "id_menu")
    public int getIdMenu() {
        return idMenu;
    }

    public void setIdMenu(int idMenu) {
        this.idMenu = idMenu;
    }

    @Basic
    @Column(name = "nombre")
    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    @Basic
    @Column(name = "postre")
    public String getPostre() {
        return postre;
    }

    public void setPostre(String postre) {
        this.postre = postre;
    }

    @Basic
    @Column(name = "precio")
    public double getPrecio() {
        return precio;
    }

    public void setPrecio(double precio) {
        this.precio = precio;
    }

    @Basic
    @Column(name = "fecha_creacion")
    public Date getFechaCreacion() {
        return fechaCreacion;
    }

    public void setFechaCreacion(Date fechaCreacion) {
        this.fechaCreacion = fechaCreacion;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Menu menu = (Menu) o;
        return idMenu == menu.idMenu &&
                Double.compare(menu.precio, precio) == 0 &&
                Objects.equals(nombre, menu.nombre) &&
                Objects.equals(postre, menu.postre) &&
                Objects.equals(fechaCreacion, menu.fechaCreacion);
    }

    @Override
    public int hashCode() {
        return Objects.hash(idMenu, nombre, postre, precio, fechaCreacion);
    }

    @OneToMany(mappedBy = "menu")
    public List<MenuRestaurante> getSirve() {
        return sirve;
    }

    public void setSirve(List<MenuRestaurante> sirve) {
        this.sirve = sirve;
    }

    @ManyToMany
    @JoinTable(name = "menu_plato", catalog = "", schema = "basehibernate", joinColumns = @JoinColumn(name = "id_menu", referencedColumnName = "id_menu", nullable = false), inverseJoinColumns = @JoinColumn(name = "id_plato", referencedColumnName = "id_plato", nullable = false))
    public List<Plato> getPlatos() {
        return platos;
    }

    public void setPlatos(List<Plato> platos) {
        this.platos = platos;
    }
}
