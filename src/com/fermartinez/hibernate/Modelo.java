package com.fermartinez.hibernate;

import com.fermartinez.hibernate.util.HibernateUtil;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.boot.registry.StandardServiceRegistry;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.cfg.Configuration;

import javax.persistence.Query;
import java.io.*;
import java.sql.*;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

public class Modelo {
    SessionFactory sessionFactory;

    private String ip;
    private String user;
    private String password;
    private String adminPassword;


    String getIP() {
        return ip;
    }
    String getUser() {
        return user;
    }
    String getPassword() {
        return password;
    }
    String getAdminPassword() {
        return adminPassword;
    }

    public void desconectar() {
        //Cierro la factoria de sessiones
        if(sessionFactory != null && sessionFactory.isOpen())
            sessionFactory.close();
    }

    public void conectar() {
        Configuration configuracion = new Configuration();
        //Cargo el fichero Hibernate.cfg.xml
        configuracion.configure("hibernate.cfg.xml");

        //Indico la clase mapeada con anotaciones
        configuracion.addAnnotatedClass(Cocinero.class);
        configuracion.addAnnotatedClass(Plato.class);
        configuracion.addAnnotatedClass(Menu.class);
        configuracion.addAnnotatedClass(Restaurante.class);
        configuracion.addAnnotatedClass(CocineroRestaurante.class);
        configuracion.addAnnotatedClass(MenuRestaurante.class);

        //Creamos un objeto ServiceRegistry a partir de los parámetros de configuración
        //Esta clase se usa para gestionar y proveer de acceso a servicios
        StandardServiceRegistry ssr = new StandardServiceRegistryBuilder().applySettings(
                configuracion.getProperties()).build();

        //finalmente creamos un objeto sessionfactory a partir de la configuracion y del registro de servicios
        sessionFactory = configuracion.buildSessionFactory(ssr);

    }

    public void insertarCocinero(Cocinero nuevoCocinero) {
        //Obtengo una session a partir de la factoria de sesiones
        Session sesion = sessionFactory.openSession();
        sesion.beginTransaction();
        sesion.save(nuevoCocinero);
        sesion.getTransaction().commit();
        sesion.close();
    }

    public void insertarPlato(Plato nuevoPlato) {
        //Obtengo una session a partir de la factoria de sesiones
        Session sesion = sessionFactory.openSession();
        sesion.beginTransaction();
        sesion.save(nuevoPlato);
        sesion.getTransaction().commit();
        sesion.close();
    }

    public void insertarMenu(Menu nuevoMenu) {
        //Obtengo una session a partir de la factoria de sesiones
        Session sesion = sessionFactory.openSession();
        sesion.beginTransaction();
        sesion.save(nuevoMenu);
        sesion.getTransaction().commit();
        sesion.close();
    }

    public void insertarRestaurante(Restaurante nuevoRestaurante) {
        //Obtengo una session a partir de la factoria de sesiones
        Session sesion = sessionFactory.openSession();
        sesion.beginTransaction();
        sesion.save(nuevoRestaurante);
        sesion.getTransaction().commit();
        sesion.close();
    }

    public void insertarTrabajador(CocineroRestaurante nuevoTrabajador) {
        //Obtengo una session a partir de la factoria de sesiones
        Session sesion = sessionFactory.openSession();
        sesion.beginTransaction();
        sesion.save(nuevoTrabajador);
        sesion.getTransaction().commit();
        sesion.close();
    }

    public void insertarCarta(MenuRestaurante nuevaCarta) {
        //Obtengo una session a partir de la factoria de sesiones
        Session sesion = sessionFactory.openSession();
        sesion.beginTransaction();
        sesion.save(nuevaCarta);
        sesion.getTransaction().commit();
        sesion.close();
    }

    public void modificarCocinero(Cocinero nuevoCocinero) {
        Session sesion = sessionFactory.openSession();
        sesion.beginTransaction();
        sesion.saveOrUpdate(nuevoCocinero);
        sesion.getTransaction().commit();
        sesion.close();
    }

    public void modificarPlato(Plato nuevoPlato) {
        Session sesion = sessionFactory.openSession();
        sesion.beginTransaction();
        sesion.saveOrUpdate(nuevoPlato);
        sesion.getTransaction().commit();
        sesion.close();
    }

    public void modificarMenu(Menu nuevoMenu) {
        Session sesion = sessionFactory.openSession();
        sesion.beginTransaction();
        sesion.saveOrUpdate(nuevoMenu);
        sesion.getTransaction().commit();
        sesion.close();
    }

    public void modificarRestaurante(Restaurante nuevoRestaurante) {
        Session sesion = sessionFactory.openSession();
        sesion.beginTransaction();
        sesion.saveOrUpdate(nuevoRestaurante);
        sesion.getTransaction().commit();
        sesion.close();
    }

    public void modificarTrabajador(CocineroRestaurante nuevoTrabajador) {
        Session sesion = sessionFactory.openSession();
        sesion.beginTransaction();
        sesion.saveOrUpdate(nuevoTrabajador);
        sesion.getTransaction().commit();
        sesion.close();
    }

    public void modificarCarta(MenuRestaurante nuevaCarta) {
        Session sesion = sessionFactory.openSession();
        sesion.beginTransaction();
        sesion.saveOrUpdate(nuevaCarta);
        sesion.getTransaction().commit();
        sesion.close();
    }



    public void borrarCocinero(Cocinero cocineroBorrado) {
        Session sesion = sessionFactory.openSession();
        sesion.beginTransaction();
        sesion.delete(cocineroBorrado);
        sesion.getTransaction().commit();
        sesion.close();
    }

    public void borrarPlato(Plato platoBorrado) {
        Session sesion = sessionFactory.openSession();
        sesion.beginTransaction();
        sesion.delete(platoBorrado);
        sesion.getTransaction().commit();
        sesion.close();
    }

    public void borrarMenu(Menu menuBorrado) {
        Session sesion = sessionFactory.openSession();
        sesion.beginTransaction();
        sesion.delete(menuBorrado);
        sesion.getTransaction().commit();
        sesion.close();
    }

    public void borrarRestaurante(Restaurante restauranteBorrado) {
        Session sesion = sessionFactory.openSession();
        sesion.beginTransaction();
        sesion.delete(restauranteBorrado);
        sesion.getTransaction().commit();
        sesion.close();
    }

    public void borrarTrabajador(CocineroRestaurante trabajadorBorrado) {
        Session sesion = sessionFactory.openSession();
        sesion.beginTransaction();
        sesion.delete(trabajadorBorrado);
        sesion.getTransaction().commit();
        sesion.close();
    }

    public void borrarCarta(MenuRestaurante cartaBorrada) {
        Session sesion = sessionFactory.openSession();
        sesion.beginTransaction();
        sesion.delete(cartaBorrada);
        sesion.getTransaction().commit();
        sesion.close();
    }



    /**
     * Lee el archivo de propiedades y setea los atributos pertinentes
     */
    private void getPropValues() {
        InputStream inputStream = null;
        try {
            Properties prop = new Properties();
            String propFileName = "config.properties";

            inputStream = new FileInputStream(propFileName);

            prop.load(inputStream);
            ip = prop.getProperty("ip");
            user = prop.getProperty("user");
            password = prop.getProperty("pass");
            adminPassword = prop.getProperty("admin");

        } catch (Exception e) {
            System.out.println("Exception: " + e);
        } finally {
            try {
                if (inputStream != null) inputStream.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    /**
     * Actualiza las propiedades pasadas por parámetro del archivo de propiedades
     *
     * @param ip   ip de la bbdd
     * @param user user de la bbdd
     * @param pass contraseña de la bbdd
     * @param adminPass contraseña del administrador
     */
    void setPropValues(String ip, String user, String pass, String adminPass) {
        try {
            Properties prop = new Properties();
            prop.setProperty("ip", ip);
            prop.setProperty("user", user);
            prop.setProperty("pass", pass);
            prop.setProperty("admin", adminPass);
            OutputStream out = new FileOutputStream("config.properties");
            prop.store(out, null);

        } catch (IOException ex) {
            ex.printStackTrace();
        }
        this.ip = ip;
        this.user = user;
        this.password = pass;
        this.adminPassword = adminPass;
    }

    public ArrayList<Cocinero> getCocineros() {
        Session sesion = sessionFactory.openSession();
        javax.persistence.Query query = sesion.createQuery("FROM Cocinero");
        ArrayList<Cocinero> listaCocineros = (ArrayList<Cocinero>)query.getResultList();
        sesion.close();
        return listaCocineros;
    }

    public ArrayList<Menu> getMenus(Menu menuSeleccionado) {
        Session sesion = sessionFactory.openSession();
        Query query = sesion.createQuery("FROM Coche WHERE propietario = :prop");
        query.setParameter("prop", menuSeleccionado);
        ArrayList<Menu> lista = (ArrayList<Menu>) query.getResultList();
        sesion.close();
        return lista;
    }
}
