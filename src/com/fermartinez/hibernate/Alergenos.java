package com.fermartinez.hibernate;

public enum Alergenos {

    ALERGENOSGLUTEN("Cereales con gluten"),
    ALERGENOSCRUSTACEOS("Crustáceos"),
    ALERGENOSHUEVO("Huevo"),
    ALERGENOSPESCADO("Pescado"),
    ALERGENOSCACAHUETES("Cacahuetes"),
    ALERGENOSSOJA("Soja"),
    ALERGENOSLACTEOS("Lácteos"),
    ALERGENOSFRUTOSSECOS("Frutos secos"),
    ALERGENOSAPIO("Apio"),
    ALERGENOSMOSTAZA("Mostaza"),
    ALERGENOSSESAMO("Sésamo"),
    ALERGENOSSULFITOS("Sulfitos"),
    ALERGENOSALTRAMUCES("Altramuces"),
    ALERGENOSMOLUSCOS("Moluscos");


    private String valor;

    Alergenos(String valor) {
        this.valor = valor;
    }

    public String getValor() {
        return valor;
    }
}
