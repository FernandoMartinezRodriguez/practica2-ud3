package com.fermartinez.hibernate;

public enum Estrellas {

    ESTRELLAS1("1 estrellas"),
    ESTRELLAS2("2 estrellas"),
    ESTRELLAS3("3 estrellas"),
    ESTRELLAS4("4 estrellas"),
    ESTRELLAS5("5 estrellas");


    private String valor;

    Estrellas(String valor) {
        this.valor = valor;
    }

    public String getValor() {
        return valor;
    }
}
